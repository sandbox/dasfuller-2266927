CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


Introduction
------------
The Cards Against Humanity module creates a block that rando cardrissians
black and white cards from the popular card game. That's it.


Requirements
------------
This module requires the following modules:
* Block


Installation
------------
Install as you would normally install a contributed drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


Configuration
-------------
No configuration necessary.


MAINTAINERS
-----------
Current maintainers:
* Adam Fuller (dasfuller) - https://drupal.org/user/2731951
